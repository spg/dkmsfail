SOME_VAR:=SOME_VALUE

obj-m += dkmsfail.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) \
		CFLAGS_MODULE='-DSOME_VAR=\"$(SOME_VAR)\"' modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
