#include <linux/module.h>
#include <linux/kernel.h>

int init_module(void)
{
  printk(KERN_INFO "The value of variable is " SOME_VAR "\n");
  return 0;
}

void cleanup_module(void)
{
  printk(KERN_INFO "The value of variable was " SOME_VAR "\n");
}
